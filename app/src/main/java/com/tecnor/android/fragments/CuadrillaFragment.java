package com.tecnor.android.fragments;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.tecnor.android.R;
import com.tecnor.android.activities.LoginActivity;
import com.tecnor.android.adapters.EmpleadosAdapter;
import com.tecnor.android.api.Api;
import com.tecnor.android.api.RequestHandler;
import com.tecnor.android.models.Empleados;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;
import io.realm.RealmResults;

import com.tecnor.android.fragments.OrdenesFragment;
import com.tecnor.android.realm.models.Empleado;
import com.tecnor.android.utils.Formatters;
import com.tecnor.android.utils.ResponseType;

/**
 * A simple {@link Fragment} subclass.
 */
public class CuadrillaFragment extends Fragment {

    private static final String TEXT = "text";

    private RecyclerView mRecyclerView;
    private EmpleadosAdapter  mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    List<Empleados> empleadosList = new ArrayList<>();
    public SweetAlertDialog alertSeg;

    private Realm realm;
    private RealmResults<Empleado> empleadosSel;
    private Empleado empleado;

    public static CuadrillaFragment newInstance(String text) {
        CuadrillaFragment frag = new CuadrillaFragment();

        Bundle args = new Bundle();
        args.putString(TEXT, text);
        frag.setArguments(args);
        return frag;
    }

    public CuadrillaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cuadrilla, container, false);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerViewCuadrilla);
        Realm.init(getContext());
        readEmpleados();
        view.findViewById(R.id.buttonCuadrilla).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goOrders();
            }
        });

        return view;
    }

    private void readEmpleados()
    {
        alertSeg = new   SweetAlertDialog( getContext(), SweetAlertDialog.PROGRESS_TYPE).setTitleText("Cargando Empleados");
        alertSeg.show();
        Ion.with(this)
                .load(Api.JSON_EMPLEADOS)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                        //Toast.makeText(getContext(),  result.toString(), Toast.LENGTH_LONG).show();
                        if (result != null) {

                            ResponseType respuesta = new ResponseType();
                            if(respuesta.getResponse(result.get("code").toString()))
                            {
                                Gson gson = new Gson();
                                String json = gson.toJson(result.get("data").toString());
                                Formatters jsonString = new Formatters();
                                try {

                                    JSONObject obj = new JSONObject(jsonString.formatJson(json));
                                    refreshContenidoList(obj.getJSONArray("technicals"));
                                    //Toast.makeText(getContext(), obj.getString("technicals"), Toast.LENGTH_LONG).show();

                                } catch (Throwable t) {
                                    Toast.makeText(getContext(), "Could not parse malformed JSON: \"" + json + "\"", Toast.LENGTH_LONG).show();
                                }

                            }else
                            {
                                Toast.makeText(getContext(), result.get("message").toString(), Toast.LENGTH_LONG).show();
                            }
                        }
                        else
                        {
                            Toast.makeText(getContext(), "No hay datos encontrados", Toast.LENGTH_LONG).show();
                        }

                        alertSeg.hide();

                    }
                });

    }



    private void refreshContenidoList(JSONArray contenido) throws JSONException
    {
        //limpiar las tiendas anteriores
        empleadosList.clear();
        //recorrer todos los elementos de la matriz json del que recibimos respuetsa
        for(int i = 0; i < contenido.length(); i++)
        {
            //obtener el json de productos
            JSONObject obj = contenido.getJSONObject(i);

            //añadiendo las categorias de JSON a clase tiendas
            //Toast.makeText(getContext(), obj.getString("name"), Toast.LENGTH_LONG).show();
            empleadosList.add(new Empleados(
                    obj.getInt("id"),
                    obj.getString("name"),
                   0
            ));
        }

        //crear el adaptador y configurar en la vista nuestra lista de informacion que llega en el formato json
        mLayoutManager = new LinearLayoutManager(getContext());
        //mAdapter = new TodosSegAdapter(todosList, R.layout.card_view_seg_nuevos );
        mAdapter = new EmpleadosAdapter(empleadosList, R.layout.card_view_config_cuadrilla, new EmpleadosAdapter.OnClickListener() {
            @Override
            public void onItemClick(Empleados empleados, int position) {


            }
            @Override
            public void asignarClick(final Empleados empleados, int position) {



                if(empleados.getSeleccionado() == 1)
                {
                    empleados.setSeleccionado(0);
                    delEmp(empleados.getId());
                }else
                {
                    empleados.setSeleccionado(1);
                    saveEmp(empleados.getId(), empleados.getNombre(), empleados.getSeleccionado());
                }
                //refreshCampos();
            }

        }, new EmpleadosAdapter.OnLongClickListener() {
            @Override
            public void onItemClick(Empleados empleados, int position) {
                //Toast.makeText(MainActivity.this, "Click largo", Toast.LENGTH_SHORT).show();
            }
        });
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }
    private void saveEmp(int emp_id, String nombre, int seleccionado)
    {
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Empleado empleado = new Empleado(emp_id, nombre, "puesto", seleccionado);
        realm.copyToRealmOrUpdate(empleado);
        realm.commitTransaction();
        refreshCampos();
    }

    private void delEmp(int id)
    {
        realm = Realm.getDefaultInstance();
        final int emp_id = id;
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                final Empleado empleadoFind = realm.where(Empleado.class).equalTo("emp_id", emp_id).findFirst();
                empleadoFind.deleteFromRealm();
            }
        });
        refreshCampos();
    }

    private void refreshCampos()
    {
        mAdapter.updateList(empleadosList);
        mAdapter.notifyDataSetChanged();
    }

    private void goOrders()
    {
        //Toast.makeText(getContext(), "Ir a las ordenes", Toast.LENGTH_LONG).show();
        //Toast.makeText(getContext(), "Ir a las ordenes", Toast.LENGTH_LONG).show();
        int title;
        title = R.string.menu_cuadrillas;
        ViewCuadrillaFragment vCuadrilla= new ViewCuadrillaFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.home_content, vCuadrilla, "findThisFragment")
                .addToBackStack(null)
                .commit();
        getActivity().setTitle(getString(title));


    }

}
