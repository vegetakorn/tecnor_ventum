package com.tecnor.android.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tecnor.android.R;
import com.tecnor.android.models.Ordenes;
import java.util.List;

public class OrdenesAdapter extends RecyclerView.Adapter<OrdenesAdapter.ViewHolder>
{
    public List<Ordenes> ordenes;
    private int layout;
    private OnClickListener listener;
    private OnLongClickListener listenerLong;
    private Context context;

    public OrdenesAdapter(List<Ordenes> ordenes, int layout, OnClickListener listener, OnLongClickListener listenerLong)
    {
        this.ordenes = ordenes;
        this.layout = layout;
        this.listener = listener;
        this.listenerLong = listenerLong;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        context = parent.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder( ViewHolder holder, int position) {

        holder.bind(ordenes.get(position), listener, listenerLong);
    }

    @Override
    public int getItemCount() {
        return ordenes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView nombre;

        public ViewHolder(View v)
        {
            super(v);
            nombre = (TextView)itemView.findViewById(R.id.textViewOrder);
        }

        public void bind(final Ordenes ordenes, final OnClickListener listener, final OnLongClickListener listenerLong){

            nombre.setText(ordenes.getNombre());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(ordenes, getAdapterPosition());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listenerLong.onItemClick(ordenes, getAdapterPosition());
                    return true;
                }
            });
        }
    }

    public interface OnClickListener{
        void onItemClick(Ordenes ordenes, int position);
    }

    public interface OnLongClickListener{
        void onItemClick(Ordenes ordenes, int position);
    }
}