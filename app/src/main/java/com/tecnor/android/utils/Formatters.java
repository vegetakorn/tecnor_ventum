package com.tecnor.android.utils;

import org.json.JSONException;
import org.json.JSONObject;

public class Formatters {

    public String formatJson(String json)
    {
        //delete backslashes ( \ ) :
        json = json.replaceAll("[\\\\]{1}[\"]{1}","\"");
        //delete first and last double quotation ( " ) :
        json = json.substring(json.indexOf("{"),json.lastIndexOf("}")+1);
        try {
            JSONObject jsonNew = new JSONObject(json);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return json;
    }

}
