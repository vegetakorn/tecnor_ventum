package com.tecnor.android.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tecnor.android.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class HomeFragment extends Fragment {


    private static final String TEXT = "text";
    public SweetAlertDialog alertSeg;

    public static HomeFragment newInstance(String text) {
        HomeFragment frag = new HomeFragment();

        Bundle args = new Bundle();
        args.putString(TEXT, text);
        frag.setArguments(args);
        return frag;
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        view.findViewById(R.id.buttonGoCuadrilla).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goViewCuadrilla();
            }
        });
        return view;
    }

    private void goViewCuadrilla()
    {
        //Toast.makeText(getContext(), "Ir a las ordenes", Toast.LENGTH_LONG).show();
        int title;
        title = R.string.menu_cuadrillas;
        ViewCuadrillaFragment vCuadrilla= new ViewCuadrillaFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.home_content, vCuadrilla, "findThisFragment")
                .addToBackStack(null)
                .commit();
        getActivity().setTitle(getString(title));
    }

}
