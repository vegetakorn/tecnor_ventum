package com.tecnor.android.api;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class RequestHandler {

    //metodo para enviar httpPostRequest

    public String sendPostRequest(String requestUrl, HashMap<String, String> postParams)
    {
        //creacion de una URL
        URL url;

        //objeto string para almacenar el mensaje recuperado del servidor
        StringBuilder sb = new StringBuilder();

        try{
            url = new URL(requestUrl);

            //crear conexion httpUrl
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            //configuracion de propiedades de conexion
            conn.setReadTimeout(60000);
            conn.setConnectTimeout(60000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            conn.setRequestProperty("Accept","application/json");
            conn.setRequestProperty("X-Requested-With","XMLHttpRequest");
            conn.setRequestProperty("Authorization","Bearer TOKEN");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            //creacion de flujo de salida
            OutputStream os = conn.getOutputStream();

            //escribir parametros de la peticion
            //utilizar un metodo get o post data string que define a continuacion
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));

            writer.write(getPostDataString(postParams));
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if(responseCode == HttpsURLConnection.HTTP_OK)
            {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                sb = new StringBuilder();
                String response;

                //leer al servidor
                while((response = br.readLine()) != null)
                {
                    sb.append(response);
                }
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return sb.toString();

    }

    public String sendGetRequest(String requestUrl)
    {
        StringBuilder sb = new StringBuilder();
        try
        {

            URL url = new URL(requestUrl);

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String s;
            while ((s = bufferedReader.readLine()) != null )
            {
                sb.append(s + "\n");
            }

        }catch(Exception e)
        {
            e.printStackTrace();
        }

        return sb.toString();
    }

    private String getPostDataString(HashMap<String, String> params)throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();

        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet())
        {
            if(first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));

        }

        return result.toString();
    }

}
