package com.tecnor.android.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.tecnor.android.R;
import com.tecnor.android.api.Api;
import com.tecnor.android.utils.ResponseType;
import com.tecnor.android.api.RequestHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.internal.http2.Header;


import static java.net.Proxy.Type.HTTP;

public class LoginActivity extends AppCompatActivity {

    public SweetAlertDialog alertLogin;
    private EditText usuario;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //variables de login
       usuario = (EditText) findViewById(R.id.inputUser);
       password = (EditText) findViewById(R.id.inputPass);

        findViewById(R.id.buttonLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUsuario();
            }
        });

        findViewById(R.id.textRecovery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recoveryPass();
            }
        });

        findViewById(R.id.txtRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               register();
            }
        });
    }

    public void generarPeticionLogin(){

       /* Ion.with(this)
                .load("http://trapp.arteika.com/json/orders_get.json")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                        Toast.makeText(LoginActivity.this,  result.toString(), Toast.LENGTH_LONG).show();

                    }
                });*/
        /*JsonObject data = new JsonObject();
        data.addProperty("user", "harr@gmail.com");
        data.addProperty("password", "12345");
        JsonObject json = new JsonObject();

        json.add("data", data);

        Ion.with(this)
                .load("https://trapp.arteika.com/api/v1/session/login")
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                       //
                        if (result != null) {
                            Toast.makeText(LoginActivity.this,  result.toString(), Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(LoginActivity.this,  e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                });*/
    }

    private void loginUsuario()
    {
        final String usu =  usuario.getText().toString().trim();
        final String pass = password.getText().toString().trim();

        if(TextUtils.isEmpty(usu))
        {
            usuario.setError("Debes escribir un nombre de usuario");
            usuario.requestFocus();
            return;
        }
        if(TextUtils.isEmpty(pass))
        {
            password.setError("Debes escribir una contraseña");
            password.requestFocus();
            return;
        }
        alertLogin = new   SweetAlertDialog( LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("Entrando al Sistema");
        JsonObject data = new JsonObject();
        data.addProperty("user", usu);
        data.addProperty("password", pass);
        JsonObject json = new JsonObject();
        json.add("data", data);
        alertLogin.show();

        Ion.with(this)
                .load(Api.URL_LOGIN_USUARIO)
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // do stuff with the result or error
                         if (result != null) {

                             ResponseType respuesta = new ResponseType();
                             if(respuesta.getResponse(result.get("code").toString()))
                             {
                                 goHome();
                                 //Toast.makeText(LoginActivity.this, "Si existe usuario", Toast.LENGTH_LONG).show();
                             }else
                             {
                                 Toast.makeText(LoginActivity.this, result.get("message").toString(), Toast.LENGTH_LONG).show();
                                 alertLogin.hide();
                             }
                        }
                         else
                         {
                            Toast.makeText(LoginActivity.this, "No hay datos encontrados", Toast.LENGTH_LONG).show();
                             alertLogin.hide();
                        }

                    }
                });
    }

    private void goHome()
    {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
        alertLogin.hide();
    }

    private void recoveryPass()
    {
        Intent intent = new Intent(LoginActivity.this, RecoveryActivity.class);
        startActivity(intent);
    }

    private void register()
    {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }
}
