package com.tecnor.android.activities;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tecnor.android.R;

public class RegisterActivity extends AppCompatActivity {

    private TextInputLayout textinputlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        textinputlayout = (TextInputLayout) findViewById(R.id.inputLayoutName);
        textinputlayout.setHint(textinputlayout.getHint()+" "+getString(R.string.asteriskred));
    }
}
