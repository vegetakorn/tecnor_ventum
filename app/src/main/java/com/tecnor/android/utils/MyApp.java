package com.tecnor.android.utils;

import android.app.Application;

import com.tecnor.android.realm.models.Empleado;
import com.tecnor.android.realm.models.User;

import java.util.concurrent.atomic.AtomicInteger;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;




public class MyApp extends Application {

    public static AtomicInteger UserID = new AtomicInteger();
    public static AtomicInteger EmpleadoID = new AtomicInteger();


    @Override
    public void onCreate()
    {
        setUpRealmConfig();
        Realm realm = Realm.getDefaultInstance();
        UserID = getIdByTable(realm, User.class);
        EmpleadoID = getIdByTable(realm, Empleado.class);
        realm.close();
        super.onCreate();
    }

    private void setUpRealmConfig()
    {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);
    }

    private <T extends RealmObject> AtomicInteger getIdByTable(Realm realm, Class<T> anyClass){
        RealmResults<T> results = realm.where(anyClass).findAll();
        return (results.size() > 0) ? new AtomicInteger(results.max("id").intValue()) : new AtomicInteger();
    }
}
