package com.tecnor.android.realm.models;


import com.tecnor.android.utils.MyApp;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;


public class Empleado extends RealmObject
{
    @PrimaryKey
    private int id;

    private int emp_id;
    @Required
    private String nombre;
    @Required
    private String puesto;
    private int seleccionado;

    public Empleado(){}

    public Empleado(int emp_id, String nombre,  String puesto, int seleccionado)
    {
        this.id = MyApp.EmpleadoID.incrementAndGet();
        this.emp_id = emp_id;
        this.nombre = nombre;
        this.puesto = puesto;
        this.seleccionado = seleccionado;
    }

    public int getId() {
        return id;
    }

    public int getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(int emp_id) {
        this.emp_id = emp_id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public int getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(int seleccionado) {
        this.seleccionado = seleccionado;
    }
}