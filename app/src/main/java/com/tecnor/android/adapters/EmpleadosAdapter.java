package com.tecnor.android.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tecnor.android.R;
import com.tecnor.android.models.Empleados;
import com.tecnor.android.realm.models.Empleado;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class EmpleadosAdapter extends RecyclerView.Adapter<EmpleadosAdapter.ViewHolder>
{
    public List<Empleados> empleados;
    private int layout;
    private OnClickListener listener;
    private OnLongClickListener listenerLong;
    private OnItemClickListener mListener;

    private Context context;

    public interface  OnItemClickListener
    {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener)
    {
        mListener = listener;
    }


    public EmpleadosAdapter(List<Empleados> empleados, int layout, OnClickListener listener, OnLongClickListener listenerLong)
    {
        this.empleados = empleados;
        this.layout = layout;
        this.listener = listener;
        this.listenerLong = listenerLong;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        ViewHolder vh = new ViewHolder(v);
        context = parent.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder( ViewHolder holder, int position) {

        holder.bind(empleados.get(position), listener, listenerLong);

    }


    @Override
    public int getItemCount() {
        return empleados.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView nombre;
        //private TextView totcampos;
        private LinearLayout fondoFila;
        private Button botonAsignar;
        private Realm realm;


        public ViewHolder(View v)
        {
            super(v);

            nombre = (TextView)itemView.findViewById(R.id.textViewGetEmp);
            //fondoFila = (LinearLayout)itemView.findViewById(R.id.rowLayoutCuadrilla);
            botonAsignar = (Button)itemView.findViewById(R.id.buttonAsignaEmpleado);


        }

        public void bind(final Empleados empleados, final OnClickListener listener, final OnLongClickListener listenerLong){

            nombre.setText(empleados.getNombre());
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    final RealmResults<Empleado> empList = realm.where(Empleado.class).findAll();
                    //cam.setColor("#EAE5E3");
                    for (Empleado emp : empList) {
                        if(emp.getEmp_id() == empleados.getId() )
                        {
                            empleados.setSeleccionado(1);
                        }

                    }
                }
            });

            int green = Color.parseColor("#00da90");
            int red = Color.parseColor("#EC2A5F");

            if(empleados.getSeleccionado() == 1)
            {
                botonAsignar.setBackgroundColor(red);
                botonAsignar.setText("Quitar");
            }else
            {
                botonAsignar.setBackgroundColor(green);
                botonAsignar.setText("Agregar");
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(empleados, getAdapterPosition());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listenerLong.onItemClick(empleados, getAdapterPosition());
                    return true;
                }
            });
            botonAsignar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {



                listener.asignarClick(empleados, getAdapterPosition());
                }
            });
        }
    }

    public void updateList(List<Empleados> empleados) {
        this.empleados = empleados;
        notifyDataSetChanged();
    }

    public interface OnClickListener{

        void onItemClick(Empleados empleados, int position);
        void asignarClick(Empleados empleados, int position);
    }

    public interface OnLongClickListener{

        void onItemClick(Empleados empleados, int position);
    }

}
