package com.tecnor.android.api;

public class Api {

    //url principal
    private static final String URL_BASE = "https://trapp.arteika.com/";

    //carpeta api
    public static final String ROOT_API = URL_BASE + "api/";
    //carpeta jsons
    public static final String ROOT_JSON = URL_BASE + "json/";
    //carpeta v1
    public static final String ROOT_DIR = ROOT_API + "v1/";


    //servicios
    public static final String URL_LOGIN_USUARIO = ROOT_DIR + "session/login";
    public static final String URL_TOKEN_USUARIO = ROOT_DIR + "session/token";
    public static final String URL_LOGOUT_USUARIO = ROOT_DIR + "session/logout";

    //json precargados
    public static final String JSON_EMPLEADOS = ROOT_JSON + "technicals_get.json";
    public static final String URL_EMPLEADOS = ROOT_DIR + "empleados";


}
