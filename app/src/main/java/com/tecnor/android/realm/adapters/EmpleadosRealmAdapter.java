package com.tecnor.android.realm.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tecnor.android.R;
import com.tecnor.android.realm.models.Empleado;

import java.util.List;

import io.realm.Realm;

public class EmpleadosRealmAdapter extends RecyclerView.Adapter<EmpleadosRealmAdapter.ViewHolder>
{
    public List<Empleado> empleados;
    private int layout;
    private EmpleadosRealmAdapter.OnClickListener listener;
    private EmpleadosRealmAdapter.OnLongClickListener listenerLong;
    private EmpleadosRealmAdapter.OnItemClickListener mListener;
    private Realm realm;

    private Context context;

    public interface  OnItemClickListener
    {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(EmpleadosRealmAdapter.OnItemClickListener listener)
    {
        mListener = listener;
    }


    public EmpleadosRealmAdapter(List<Empleado> empleados, int layout, EmpleadosRealmAdapter.OnClickListener listener, EmpleadosRealmAdapter.OnLongClickListener listenerLong)
    {
        this.empleados = empleados;
        this.layout = layout;
        this.listener = listener;
        this.listenerLong = listenerLong;
    }

    @Override
    public EmpleadosRealmAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        EmpleadosRealmAdapter.ViewHolder vh = new EmpleadosRealmAdapter.ViewHolder(v);
        context = parent.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(EmpleadosRealmAdapter.ViewHolder holder, int position) {

        holder.bind(empleados.get(position), listener, listenerLong);

    }


    @Override
    public int getItemCount() {
        return empleados.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView nombre;
        //private TextView totcampos;
        private LinearLayout fondoFila;
        private Button botonAsignar;



        public ViewHolder(View v)
        {
            super(v);

            nombre = (TextView)itemView.findViewById(R.id.textViewGetEmpSel);
            //fondoFila = (LinearLayout)itemView.findViewById(R.id.rowLayoutCuadrilla);
            botonAsignar = (Button)itemView.findViewById(R.id.buttonQuitaEmpleado);

        }

        public void bind(final Empleado empleados, final EmpleadosRealmAdapter.OnClickListener listener, final EmpleadosRealmAdapter.OnLongClickListener listenerLong){

            nombre.setText(empleados.getNombre());

            int green = Color.parseColor("#00da90");
            int red = Color.parseColor("#EC2A5F");

            botonAsignar.setBackgroundColor(red);
            botonAsignar.setText("Quitar");

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(empleados, getAdapterPosition());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listenerLong.onItemClick(empleados, getAdapterPosition());
                    return true;
                }
            });
            botonAsignar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.asignarClick(empleados, getAdapterPosition());
                }
            });
        }
    }

    public void updateList(List<Empleado> empleados) {
        this.empleados = empleados;
        notifyDataSetChanged();
    }

    public interface OnClickListener{

        void onItemClick(Empleado empleados, int position);
        void asignarClick(Empleado empleados, int position);
    }

    public interface OnLongClickListener{

        void onItemClick(Empleado empleados, int position);
    }

}
