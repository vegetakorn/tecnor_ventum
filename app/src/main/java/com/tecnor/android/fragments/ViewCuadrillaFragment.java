package com.tecnor.android.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.tecnor.android.R;
import com.tecnor.android.adapters.EmpleadosAdapter;
import com.tecnor.android.models.Empleados;
import com.tecnor.android.realm.adapters.EmpleadosRealmAdapter;
import com.tecnor.android.realm.models.Empleado;

import java.io.ByteArrayOutputStream;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewCuadrillaFragment extends Fragment {

    private static final String TEXT = "text";
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    public SweetAlertDialog alertSeg;
    public SweetAlertDialog alertErr;
    public String foto_nombre = "nofoto";
    public String foto_base64;
    private ImageView foto;
    private EditText economico;
    private Button guardar;
    private Button agregar;

    private RecyclerView mRecyclerView;
    private EmpleadosRealmAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Realm realm;
    private RealmResults<Empleado> empleadosList;

    public static ViewCuadrillaFragment newInstance(String text) {
        ViewCuadrillaFragment frag = new ViewCuadrillaFragment();

        Bundle args = new Bundle();
        args.putString(TEXT, text);
        frag.setArguments(args);
        return frag;
    }

    public ViewCuadrillaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_cuadrilla, container, false);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerViewCuadrillaSel);
        Realm.init(getContext());
        realm = Realm.getDefaultInstance();
        foto = (ImageView) view.findViewById(R.id.imagenViewCampo);
        economico = (EditText) view.findViewById(R.id.textNoEconomico);
        /**aqui se llena el cardview de empleados seleccionados**/
        //empleadosList = realm.where(Empleado.class).equalTo("sincronizado", 0).equalTo("status", 133).findAll();
        empleadosList = realm.where(Empleado.class).findAll();



        //empleadosList.addChangeListener((OrderedRealmCollectionChangeListener<RealmResults<Empleado>>) this);

        //crear el adaptador y configurar en la vista nuestra lista de informacion que llega en el formato json
        mLayoutManager = new LinearLayoutManager(getContext());
        //mAdapter = new TodosSegAdapter(todosList, R.layout.card_view_seg_nuevos );
        mAdapter = new EmpleadosRealmAdapter(empleadosList, R.layout.card_view_cuadrilla_sel, new EmpleadosRealmAdapter.OnClickListener() {
            @Override
            public void onItemClick(Empleado empleados, int position) {


            }
            @Override
            public void asignarClick(Empleado empleados, int position) {

                delEmp(empleados.getEmp_id());
                //refreshCampos();
            }

        }, new EmpleadosRealmAdapter.OnLongClickListener() {
            @Override
            public void onItemClick(Empleado empleados, int position) {
                //Toast.makeText(MainActivity.this, "Click largo", Toast.LENGTH_SHORT).show();
            }
        });
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        /**fin de empleados seleccionados**/

        agregar = (Button) view.findViewById(R.id.buttonAddEmpleados);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goEmpleados();
            }
        });
        guardar = (Button) view.findViewById(R.id.buttonSaveCuadrilla);
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validarCuadrilla();
                //Toast.makeText(getContext(), "Subida de información de cuadrilla", Toast.LENGTH_LONG).show();
            }
        });

        view.findViewById(R.id.imagenViewCampo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               tomarFoto();
            }
        });

        return view;
    }

    private void delEmp(int id)
    {
        realm = Realm.getDefaultInstance();
        final int emp_id = id;
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                final Empleado empleadoFind = realm.where(Empleado.class).equalTo("emp_id", emp_id).findFirst();
                empleadoFind.deleteFromRealm();
            }
        });
        refreshCampos();
    }


    private void refreshCampos()
    {
        mAdapter.updateList(empleadosList);
        mAdapter.notifyDataSetChanged();
    }

    private void tomarFoto()
    {
       /* if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(getContext(), "No tienes permiso para tomar foto", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA}, 0);

        }else
        {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, 0);
        }*/
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        //3434 es un int que esperaremos como resultado una vez sea tomada la foto.
        startActivityForResult(intent, 3434);
       // Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
       // startActivityForResult(intent, 3434);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap imagen = (Bitmap) data.getExtras().get("data");

        Bitmap bitmap=(Bitmap)data.getExtras().get("data");
        //transformamos la imagen a base 64, para enviarla al servicio web
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        //vamos a nombrar el archivo
        foto_nombre = "FotoGrua";
        foto_base64 = base64String;

        byte[] code = Base64.decode(foto_base64, Base64.DEFAULT);
        Bitmap bitMap = BitmapFactory.decodeByteArray(code, 0,code.length);

        Drawable d = new BitmapDrawable(getResources(), bitmap);
      //  foto.setImageDrawable(getResources().getDrawable(R.drawable.alo));
        foto.setBackground(d );
        foto.setAlpha(0);
        //foto.setImageBitmap(bitMap);
        //Este metodo nos retornara la url temporal de la imagen tomada
        //Uri ulrImagen = data.getData();
        // cropCapturedImage(ulrImagen);
    }

    public void cropCapturedImage(Uri urlImagen){

        //inicializamos nuestro intent
        Intent cropIntent = new Intent("com.android.camera.action.CROP");

        cropIntent.setDataAndType(urlImagen, "image/*");

        //Habilitamos el crop en este intent
        cropIntent.putExtra("crop", "true");

        cropIntent.putExtra("aspectX", 1);
        cropIntent.putExtra("aspectY", 1);

        //indicamos los limites de nuestra imagen a cortar
        cropIntent.putExtra("outputX", 400);
        cropIntent.putExtra("outputY", 250);

        //True: retornara la imagen como un bitmap, False: retornara la url de la imagen la guardada.
        cropIntent.putExtra("return-data", true);

        //iniciamos nuestra activity y pasamos un codigo de respuesta.
        startActivityForResult(cropIntent, 3535);
    }

    private void goEmpleados()
    {
        //Toast.makeText(getContext(), "Ir a las ordenes", Toast.LENGTH_LONG).show();
        int title;
        title = R.string.msg_crear_cuadrilla;
        CuadrillaFragment cuadrillas= new CuadrillaFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.home_content, cuadrillas, "findThisFragment")
                .addToBackStack(null)
                .commit();
        getActivity().setTitle(getString(title));
    }

    private void validarCuadrilla()
    {
        final String usu =  economico.getText().toString().trim();
        if(TextUtils.isEmpty(usu))
        {
            economico.setError("Debes definir el número económico");
            economico.requestFocus();
            return;
        }
        if(empleadosList.size() == 0)
        {
            Toast.makeText(getContext(), "No tienes empleados seleccionados", Toast.LENGTH_LONG).show();
            return;
        }
        if(foto_nombre == "nofoto")
        {
            Toast.makeText(getContext(), "No tienes foto de grua capturada", Toast.LENGTH_LONG).show();
            return;
        }

        int title;
        title = R.string.menu_orders;
        OrdenesFragment orders= new OrdenesFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.home_content, orders, "findThisFragment")
                .addToBackStack(null)
                .commit();
        getActivity().setTitle(getString(title));

    }


}
