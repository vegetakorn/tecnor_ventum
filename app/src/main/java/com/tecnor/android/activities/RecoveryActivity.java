package com.tecnor.android.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.tecnor.android.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RecoveryActivity extends AppCompatActivity {

    public SweetAlertDialog alertLogin;
    private EditText usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recovery);

        usuario = (EditText) findViewById(R.id.inputRecovery);

        findViewById(R.id.buttonRecovery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                recovery();
            }
        });
    }

    private void recovery()
    {
        final String usu =  usuario.getText().toString().trim();

        if(TextUtils.isEmpty(usu))
        {
            usuario.setError("Debes escribir un nombre de usuario");
            usuario.requestFocus();
            return;
        }

        // alertLogin = new   SweetAlertDialog( RecoveryActivity.this, SweetAlertDialog.PROGRESS_TYPE).setTitleText("");

        Intent intent = new Intent(RecoveryActivity.this, LoginActivity.class);
        startActivity(intent);

    }
}
