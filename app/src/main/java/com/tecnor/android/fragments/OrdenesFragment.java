package com.tecnor.android.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tecnor.android.R;
import com.tecnor.android.adapters.EmpleadosAdapter;
import com.tecnor.android.adapters.OrdenesAdapter;
import com.tecnor.android.models.Empleados;
import com.tecnor.android.models.Ordenes;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class OrdenesFragment extends Fragment {
    private static final String TEXT = "text";
    private RecyclerView mRecyclerView;
    private OrdenesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    List<Ordenes> ordenesList = new ArrayList<>();
    public SweetAlertDialog alertSeg;

    public static OrdenesFragment newInstance(String text) {
        OrdenesFragment frag = new OrdenesFragment();

        Bundle args = new Bundle();
        args.putString(TEXT, text);
        frag.setArguments(args);
        return frag;
    }

    public OrdenesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ordenes, container, false);
        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerViewOrdenes);
        ordenesList.clear();

        ordenesList.add(new Ordenes(1,"Orden 1"));
        ordenesList.add(new Ordenes(2,"Orden 2"));
        ordenesList.add(new Ordenes(3,"Orden 3"));
        ordenesList.add(new Ordenes(4,"Orden 4"));
        ordenesList.add(new Ordenes(5,"Orden 5"));
        readOrders();

        return view;
    }

    private void readOrders()
    {


        //crear el adaptador y configurar en la vista nuestra lista de informacion que llega en el formato json
        mLayoutManager = new LinearLayoutManager(getContext());
        //mAdapter = new TodosSegAdapter(todosList, R.layout.card_view_seg_nuevos );
        mAdapter = new OrdenesAdapter(ordenesList, R.layout.card_view_ordenes, new OrdenesAdapter.OnClickListener() {
            @Override
            public void onItemClick(Ordenes ordenes, int position) {


            }

        }, new OrdenesAdapter.OnLongClickListener() {
            @Override
            public void onItemClick(Ordenes ordenes, int position) {
                //Toast.makeText(MainActivity.this, "Click largo", Toast.LENGTH_SHORT).show();
            }
        });
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

    }

}
