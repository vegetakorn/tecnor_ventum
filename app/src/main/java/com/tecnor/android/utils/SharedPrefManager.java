package com.tecnor.android.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;


public class SharedPrefManager {

    private static final String SHARED_PREF_NAME = "ventum";
    private static final String KEY_ID = "keyid";
    private static final String KEY_NAME = "keyname";
    private static final String KEY_EMPRESA = "keyempresas_Id";
    private static final String KEY_LOGO = "keyimgEmp";
    private static final String KEY_MAIL = "keyemail";
    //keys de tienda seleccionada
    private static final String KEY_SUC = "keytienda";
    private static final String KEY_NAME_SUC = "keytiendaname";
    //keys de checklist seleccionado
    private static final String KEY_CHK = "keychecklist";
    //key del id de la visita
    private static final String KEY_VIS = "keyvisita";
    //key id de la categoria seleccionada
    private static final String KEY_CAT = "keycat";

    //key de latitud y longitud
    private static final String KEY_LAT = "keylat";
    private static final String KEY_LON = "keylon";

    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private SharedPrefManager(Context context)
    {
        mCtx = context;
    }


    public static synchronized SharedPrefManager getmInstance(Context context)
    {
        if(mInstance == null)
        {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }
  /*  public void userLogin(Usuarios user)
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_ID, user.getId());
        editor.putString(KEY_NAME, user.getName());
        editor.putInt(KEY_EMPRESA, user.getEmpresas_Id());
        editor.putString(KEY_LOGO, user.getImgEmp());
        editor.putString(KEY_MAIL, user.getEmail());
        editor.apply();
    }
    public void sucursalSel(Integer id)
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_SUC, id);
        editor.apply();
    }
    public void sucursalNameSel(String name)
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_NAME_SUC, name);
        editor.apply();
    }

    public void latitudSel(String lat)
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_LAT, lat);
        editor.apply();
    }

    public void longitudSel(String lon)
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_LON, lon);
        editor.apply();
    }

    public void checklistSel(Integer id)
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_CHK, id);
        editor.apply();
    }
    public void visitaSel(Integer id)
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_VIS, id);
        editor.apply();
    }
    public void categoriaSel(Integer id)
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_CAT, id);
        editor.apply();
    }




    //este metodo verificara si el usuario ha iniciado sesion o no
    public boolean isLoggedIn()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_NAME, null) != null;
    }
    //este metodo nos dara el usuario conectado
    public Usuarios getUser()
    {

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new Usuarios(
                sharedPreferences.getInt(KEY_ID, -1),
                sharedPreferences.getString(KEY_NAME, null),
                sharedPreferences.getString(KEY_MAIL,null),
                sharedPreferences.getString(KEY_LOGO,null),
                sharedPreferences.getInt(KEY_EMPRESA, -1)
        );
    }
    //este metodo nos dara la tienda seleccionada
    public int getSucursal()
    {

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_SUC, -1);
    }

    public String getSucursalName()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_NAME_SUC, null);
    }


    //este metodo nos dara el checklist seleccionado
    public int getChecklist()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_CHK, -1);
    }
    //este metodo nos dara el id de la visita realizada.
    public int getVisita()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_VIS, -1);
    }
    //este metodo nos dara el id de la categoria
    public int getCategoria()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(KEY_CAT, -1);
    }


    //este metodo nos dara el id de la categoria
    public String getLat()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_LAT, null);
    }
    public String getLon()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_LON, null);
    }
    //este metodo servira para desconectar al usuario
    public void logOut()
    {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        mCtx.startActivity(new Intent(mCtx, LoginActivity.class));
    }*/
}
