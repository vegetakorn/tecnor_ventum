package com.tecnor.android.realm.models;

import com.tecnor.android.utils.MyApp;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;


public class User extends RealmObject
{
    @PrimaryKey
    private int id;

    private int user_id;
    @Required
    private String user;
    @Required
    private String password;
    private String token;

    public User(){}

    public User(int user_id, String user,  String password, String token)
    {
        this.id = MyApp.UserID.incrementAndGet();
        this.user_id = user_id;
        this.user = user;
        this.password = password;
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
