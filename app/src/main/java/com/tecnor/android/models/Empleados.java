package com.tecnor.android.models;

public class Empleados {

    private int id;
    private String nombre;
    private int seleccionado;


    public Empleados(int id, String nombre, int seleccionado)
    {
        this.id = id;
        this.nombre = nombre;
        this.seleccionado = seleccionado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(int seleccionado) {
        this.seleccionado = seleccionado;
    }
}
